var jwt = require('jsonwebtoken');
var BPromise = require('bluebird');
var apigee = require('apigee-access');
var dhr = require('./dhr');

var logger = require('winston');
logger.level = 'warn';

var COOKIE_NAME = 'browser.presence';
var SECRET = '7PBdQH4HYPh3YkXGUcYtenrhAPhJUtQTLyCCDk3vWjEnIiQCFU5eIjRorNZ6gceLEef24Md69wgpLqdihsc1y5Ynsctvi8Po9J9N';
var TOKEN_VALIDITY_DURATION_IN_MIN = 20; // time in minutes within which the token is valid  

var execute = function (req, res) {
    
//	logger.debug('execute(): start');
    var oldToken = req.query.bpt || req.cookies[COOKIE_NAME];
    
    //logger.debug('oldToken: '+JSON.stringify(oldToken));
    var token_data = {
		token: {}, 
		isExpired: true,
		userEvents: req.body.data
	};
    
    if (oldToken) {
        jwt.verify(oldToken, SECRET, function (err, token) {
        	var token_data;
        	var token_stored_data = {};
        	
        	if (typeof token === 'undefined') { 
        		try {
        			token = jwt.decode(oldToken);
        		} catch (err1) {
        			logger.debug('Cannot decode the token - '+JSON.stringify(err1));
        		}
        	}
//        	logger.debug('token = '+JSON.stringify(token));
        	
        	try {
	        	if (token.data) {
	        		token_stored_data = token.data;
	        	}
        	} catch (err2) {
        		logger.debug('Cannot get token data - '+JSON.stringify(err2));
        	}
        	
        	if (err) {
            	logger.debug('err = '+JSON.stringify(err));
            	
                // Token invalid
                if (err.name === 'TokenExpiredError') {
                	token_data = {
            			data: token_stored_data, 
            			isExpired: true,
            			userEvents: req.body.data
            		};
                	
                	// If token expired just generate new one
                    dhr.execute(token_data).then(function (decision_data) {
                        respond(req, res, decision_data);
                    }, function (err) {
                        res.sendStatus(500, JSON.stringify({
                            err: err
                        }));
                    });
                } else {
                    console.error('Token verification error.');
                    respond(req, res, { result: false }, oldToken);
                }
            } else  {
            	token_data = {
            		data: token_stored_data, 
            		isExpired: false,
        			userEvents: req.body.data
        		};
            	
            	dhr.execute(token_data).then(function (decision_data) {
                    respond(req, res, decision_data, oldToken);
                }, function (err) {
                    res.sendStatus(500, JSON.stringify({
                        err: err
                    }));
                });
            }
        });
    } else {
//    	logger.debug('execute(): no token');
//    	logger.debug('now = '+now);

        dhr.execute(token_data).then(function (decision_data) {
            respond(req, res, decision_data);
        }, function (err) {
            res.sendStatus(500, JSON.stringify({
                err: err
            }));
        });
    }   
};


var respond = function (req, res, decision, oldToken) {
	
	var update_token = true;
	if (update_token) {
		// Generate new token
		generateToken(req, decision).then(function (newToken) {
	        res.send(newToken.enc);
//	        return resolve();
	        return;
		}, function (err) {
	        logger.debug(err);
	        res.sendStatus(500, JSON.stringify({
	            err: err
	        }));
	    });
	} else {
		// Send the current token
        res.send(oldToken);
//        return resolve();
    	return;
		
	}
		
};

var getNow = function () {
	return new Date().getTime();
};

var generateToken = function (req, decision) {
	var expTime = getNow() + TOKEN_VALIDITY_DURATION_IN_MIN * 60 * 1000;
	var date = new Date(expTime);
	
	var payload = {
        decisionExp: expTime,
        //decisionExpStr: date.toLocaleString(), // DEBUG data: expiration date as string 
        fp: decision.fingerprint,
        data: decision.data,        
        decision: decision.result
//        decision: false
    };

    var options = {
        expiresIn: ''+TOKEN_VALIDITY_DURATION_IN_MIN+'m',
        notBefore: 0,
        audience: 'espacenet',
        subject: apigee.getVariable(req, 'flow.epo.client.ip'),
        jwtid: apigee.getVariable(req, 'messageid')
    };

    return new BPromise(function (resolve, reject) {
        jwt.sign(payload, SECRET, options, function (token) { //sign key can be stored in Apigee Vault
            return resolve({
                enc: token,
                payload: payload
            });
        });
    });
};

exports.execute = execute;