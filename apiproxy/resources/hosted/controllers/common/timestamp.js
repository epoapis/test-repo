// *** Timestamp in seconds ***
exports.USER_EVENTS__TIMESTAMP_RATIO_FROM_MS = 1000; // The ratio for conversion from milliseconds into stored timestamp
exports.USER_EVENTS__TIMESTAMP_RATIO_INTO_SEC = 1; // The ration for conversion from a stored timestamp in seconds

// *** Timestamp in minutes ***
//exports.USER_EVENTS__TIMESTAMP_RATIO_FROM_MS = 60*1000; // The ratio for conversion from milliseconds into stored timestamp
//exports.USER_EVENTS__TIMESTAMP_RATIO_INTO_SEC = 60; // The ration for conversion from a stored timestamp in seconds

//exports.USER_EVENTS__EXPIRATION_TIME_IN_SEC = 1*60; // 1 minute in seconds
exports.USER_EVENTS__EXPIRATION_TIME_IN_SEC = 20*60; // 20 minutes in seconds
exports.USER_EVENTS__EXPIRATION_TIME = this.USER_EVENTS__EXPIRATION_TIME_IN_SEC / this.USER_EVENTS__TIMESTAMP_RATIO_INTO_SEC;

exports.getNow = function () {
	return Math.round((new Date().getTime()) / this.USER_EVENTS__TIMESTAMP_RATIO_FROM_MS);
};

exports.isTimestampDurationValid = function (duration) {
	return (duration <= this.USER_EVENTS__EXPIRATION_TIME);
};
