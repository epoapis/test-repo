var BPromise = require('bluebird');
var jwt = require('jsonwebtoken');
var lzw = require('./common/lzw');
var TimestampUtils = require('./common/timestamp');

var logger = require('winston');
logger.level = 'warn';

var USER_EVENTS__MAX_NUMBER = 5;
var USER_EVENTS__NUMBER_FOR_EXPIRED_TOKEN = 5;
var USER_EVENTS__NUMBER_FOR_NOT_EXPIRED_TOKEN = 1;

var parseUserEvents = function (data) {
	var defaultValue = {
			ts: TimestampUtils.getNow(),
			events: []
	};
	var value = defaultValue;
	
	try {
		//logger.debug('Data for parsing = '+typeof(data)+'; '+data);
		value = JSON.parse(data);
	} catch (err) {
		//logger.debug('User events parsing error '+JSON.stringify(err));
	}
	
	if (!value || typeof value.ts !== 'number'  || !Array.isArray(value.events)) {
		value = defaultValue;
	}
	return value;
};

var makeDecission = function (token_data) {
	var result = false;
	var data;
    var body;
    var userEvents = [];
    var storedUserEvents = [];
    var fingerprint;
    var serverTimestamp = TimestampUtils.getNow();
    var isExpired = true;
    
//    logger.debug('makeDecision()');
//    logger.debug('typeof(token_data) = '+typeof(token_data));
//    logger.debug('token_data = '+JSON.stringify(token_data));
    
    // Extract token data
    if (token_data) {
    	isExpired =  token_data['isExpired'] ? true : false;
//    	logger.debug('isExpired = '+isExpired);
    	
    	if (token_data) {
    		// extract stored user events
    		data = token_data['data'];
    		if (data) {
	    		try {
	    			data = lzw.decode(data);
	        		//logger.debug('stored data = '+data);
	    		} catch (err) {
		    		logger.debug('Stored user events parsing error '+JSON.stringify(err));
		    	}
	    	}
	    }
    }
    
    // Extract stored user events from the token
    storedUserEvents = parseUserEvents(data);
    
//    logger.debug('Decoding userEvents...');
    
    // Extract new user events
    if (token_data['userEvents']) {
	    try {
	        var decodedRequest = lzw.decode(token_data['userEvents']);
	        //logger.debug('decodedRequest = '+JSON.stringify(decodedRequest));
	        
	        body = JSON.parse(decodedRequest);
	        userEvents = parseUserEvents(JSON.stringify(body['a']));
	        fingerprint = body['fp'];
	        
	        //logger.debug('requestUserEvents = '+JSON.stringify(userEvents));
	        
	        var clientTimestamp = body['nts']; // The client time stamp
	        
	        // TEST This line is used for testing the difference between client and server time stamps
	        //clientTimestamp = serverTimestamp - 10;
	        
	        //logger.debug('serverTimestamp = '+serverTimestamp);
	        //logger.debug('clientTimestamp = '+clientTimestamp);
	        
	        if (typeof clientTimestamp !== 'undefined') {
	        	var timestampDifference = serverTimestamp - clientTimestamp;
	        	
	        	//logger.debug('timestampDifference = '+timestampDifference);
		        
	        	// Fix a time stamp difference between the client and the server
	        	userEvents.ts = userEvents.ts + timestampDifference;
	        }
	        
	    } catch (err) {
	        logger.debug('User events parsing error: '+JSON.stringify(err));
	    }
    }

//    logger.debug('userEvents = '+JSON.stringify(userEvents));
//    logger.debug('storedUserEvents = '+storedUserEvents.events.length+'; '+JSON.stringify(storedUserEvents));

    // Merging the stored user events and the new ones
    storedUserEvents.ts = Math.round(storedUserEvents.ts);
    userEvents.events.forEach(function (element, index, array) {
    	var duration = Math.round(userEvents.ts + element.ts) - storedUserEvents.ts;
		var newData = {
    		 ts: duration,
    		 t: element.t,
    		 x: element.x,
    		 y: element.y
    	  };
		storedUserEvents.events.splice(storedUserEvents.events.length+1,0,newData);
    });
    
//    logger.debug('storedUserEvents = '+storedUserEvents.events.length+'; '+JSON.stringify(storedUserEvents));

    // Keep only USER_EVENTS__MAX_NUMBER events
    if (storedUserEvents.events.length > USER_EVENTS__MAX_NUMBER) {
		storedUserEvents.events.splice(0,storedUserEvents.events.length-USER_EVENTS__MAX_NUMBER); // leave only USER_EVENTS__MAX_NUMBER items
	}
    
    // Switch to the new timestamp
    if (storedUserEvents.events.length > 0) {
    	var oldTs = storedUserEvents.ts;
    	var newTs = Math.round(oldTs + storedUserEvents.events[0].ts);
    	storedUserEvents.ts = newTs;
    	
    	storedUserEvents.events.forEach(function (element, index, array) {
        	var duration = Math.round(oldTs + element.ts) - newTs;
        	element.ts = duration;
        });	
	}

//    logger.debug('storedUserEvents = '+storedUserEvents.events.length+'; '+JSON.stringify(storedUserEvents));

    // Remove old elements according USER_EVENTS__EXPIRATION_TIME
    var past = serverTimestamp - TimestampUtils.USER_EVENTS__EXPIRATION_TIME; 
    var validEventCount = 0;
    storedUserEvents.events.forEach(function (element) {
    	if (storedUserEvents.ts + element.ts >= past) {
    		validEventCount++;
    	}
    });
    
    // Here we should have our robot detection logic.
    // We assume that we showed some kind of puzzle to user
    // And here we have to check how he solve it
    // For now we just check existence of any items in activity array
    
    if ( (isExpired && validEventCount >= USER_EVENTS__NUMBER_FOR_EXPIRED_TOKEN) || (!isExpired && validEventCount >= USER_EVENTS__NUMBER_FOR_NOT_EXPIRED_TOKEN) ) {
    	result = true;
    }
    
//    logger.debug('storedUserEvents = '+storedUserEvents.events.length+'; '+JSON.stringify(storedUserEvents));
//    logger.debug('result = '+result+'; validEventCount = '+validEventCount );
	
    data = JSON.stringify(storedUserEvents);
//    logger.debug('Data length = '+data.length);
	data = lzw.encode(data);
//	logger.debug('Compressed data length = '+data.length);
	
    return {
    	token_data: token_data,
        result: result, // true or false
        data: data, // user events
        fingerprint: fingerprint 
    };
};

var execute = function (token_data, reqBody) {
    return new BPromise(function (resolve, reject) { //assuming logic will be async
        resolve(makeDecission(token_data));
    });
};

exports.execute = execute;