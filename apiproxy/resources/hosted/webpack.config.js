var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: ['./frontend/utils/hdl-polyfill.js',
            './frontend/hdl.js'],
    output: {
    	path: path.resolve(__dirname,'../target/resources/app/static/js'),
        filename: 'hdl.js'
    },
    // devtool: 'source-map',
    module: {
        loaders: [
            { test: /\.css$/, loader: "style-loader!css-loader" },
            { test: /\.html$/, loader: "raw-loader" }
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
//        	sourceMap: true
//          beautify: true,
//    	  mangle: {
//    		// Skip mangling these
//    		except: ['$super', '$', 'exports', 'require']
//    	  }
        })
    ]
};