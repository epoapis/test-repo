var express = require('express');
var decisionController = require('./controllers/decisionController');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

var app = express();

app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-EPO-BrowserPresence");
   next();
});

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/static'));

app.post('/check', decisionController.execute);

app.get('*', function(req, res) {
    res.header('Content-Type', 'application/json');
    res.status(404);
    res.send(JSON.stringify({
        err: 'resource is not found'
    }));
});

app.listen(3000);
console.log('listening on port 3000');