
function base64UrlDecode(source) {
    // Append equal characters
    source = (source + '===').slice(0, source.length + (source.length % 4));
    // Replace characters that removed according to base64url specifications
    source = source.replace(/-/g, '+').replace(/_/g, '/');
    // Decode in classical base64
    source = CryptoJS.enc.Base64.parse(source);
    // Convert back from UTF-8 bytes
    return CryptoJS.enc.Utf8.stringify(source);
}

var jwt = context.getVariable('response.content');

if (jwt) {
    var encodedJwtPayload = jwt.split('.')[1];
    var jwtPayload = base64UrlDecode(encodedJwtPayload);

    context.setVariable('debug.jwtPayload', jwtPayload);

    var jwtPayloadJson = JSON.parse(jwtPayload);
    context.setVariable('flow.epo.jwt.decision', jwtPayloadJson.decision);
    context.setVariable('flow.epo.jwt.exp', jwtPayloadJson.exp);
    context.setVariable('flow.epo.jwt.iat', jwtPayloadJson.iat);
    context.setVariable('flow.epo.jwt.sub', jwtPayloadJson.sub);
    context.setVariable('flow.epo.jwt.jti', jwtPayloadJson.jti);
}
