application=robots-v2
org=epo-non-prod
environment=intg
url=https://api.enterprise.apigee.com

commit_hash=$(git log -1 --abbrev-commit | grep -m 1 commit | sed 's/commit //')
commit_date=$(git log -1 --abbrev-commit | grep -m 1 Date | sed 's/Date:   //')
commit_message="commit $commit_hash on $commit_date"

# mac related
echo find . -name .DS_Store -print0 | xargs -0 rm -rf
find . -name .DS_Store -print0 | xargs -0 rm -rf

# credentials
if [ -f .credentials ];
then
	credentials=$(head -n 1 .credentials)
else
	echo -n "your apigee username: "
	read username
	echo -n "your password please: "
	read password

	plainCredentials=$username":"$password
	credentials=$(echo -ne "$plainCredentials" | base64)
	echo $credentials > .credentials
fi

# list current revisions for this environment
curl -H "Authorization:Basic $credentials" https://api.enterprise.apigee.com/v1/o/$org/environments/$environment/apis/$application/deployments

# modifications before bundle creation

# create bundle
rm -rf $application.zip
zip -r $application.zip apiproxy

# revert modifications after bundle creation

# import bundle
curl -H "Authorization:Basic $credentials" "$url/v1/organizations/$org/apis?action=import&name=$application" -T $application.zip -H "Content-Type: application/octet-stream" -X POST

# remove bundle
rm -rf $application.zip

# deploy bundle
echo -e "\nnothing is deployed yet so you can quit now if you want."
echo -n -e "\nEnter revision to activate: "
read rev_to_deploy
curl -H "Authorization:Basic $credentials" "$url/v1/o/$org/environments/$environment/apis/$application/revisions/$rev_to_deploy/deployments?override=true&delay=5" -X POST -H "Content-Type: application/x-www-form-urlencoded"

# delete previous revision
echo -n -e "\nEnter revision to delete: "
read rev_to_delete
curl -H "Authorization:Basic $credentials" -X DELETE "$url/v1/organizations/$org/apis/$application/revisions/$rev_to_delete"
